import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.Scanner;

public class AESCipher1 {
    private static final String ALGORITHM = "AES";
    private static final String TRANSFORMATION = "AES/CBC/PKCS5Padding";
    private static final int KEY_SIZE = 256;
    private static final int IV_SIZE = 16;

    private byte[] key;

    public AESCipher1(String key) {
        try {
            this.key = hashKey(key);
        } catch (Exception e) {
            // Handle the exception (e.g., print an error message, throw a custom exception, etc.)
            e.printStackTrace();
        }
    }

    public String encrypt(String plainText) throws Exception {
        SecretKeySpec secretKeySpec = new SecretKeySpec(key, ALGORITHM);
        IvParameterSpec ivParameterSpec = generateRandomIV();

        Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);

        byte[] encryptedBytes = cipher.doFinal(plainText.getBytes(StandardCharsets.UTF_8));
        return Base64.getEncoder().encodeToString(ivParameterSpec.getIV()) + ":" +
                Base64.getEncoder().encodeToString(encryptedBytes);
    }

    public String decrypt(String cipherText) throws Exception {
        String[] parts = cipherText.split(":");
        byte[] ivBytes = Base64.getDecoder().decode(parts[0]);
        byte[] cipherBytes = Base64.getDecoder().decode(parts[1]);

        SecretKeySpec secretKeySpec = new SecretKeySpec(key, ALGORITHM);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(ivBytes);

        Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);

        byte[] decryptedBytes = cipher.doFinal(cipherBytes);
        return new String(decryptedBytes, StandardCharsets.UTF_8);
    }

    private byte[] hashKey(String key) throws Exception {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        return digest.digest(key.getBytes(StandardCharsets.UTF_8));
    }

    private IvParameterSpec generateRandomIV() {
        byte[] iv = new byte[IV_SIZE];
        SecureRandom random = new SecureRandom();
        random.nextBytes(iv);
        return new IvParameterSpec(iv);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the encryption key: ");
        String encryptionKey = scanner.nextLine();

        AESCipher1 cipher = new AESCipher1(encryptionKey);

        System.out.print("Enter plain text: ");
        String plainText = scanner.nextLine();

        try {
            String encryptedText = cipher.encrypt(plainText);
            System.out.println("Encrypted cipher text: " + encryptedText);

            String decryptedText = cipher.decrypt(encryptedText);
            System.out.println("Decrypted cipher text: " + decryptedText);
        } catch (Exception e) {
            // Handle any exceptions that occur during encryption or decryption
            e.printStackTrace();
        } finally {
            scanner.close();
        }
    }
}

