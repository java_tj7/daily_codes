class bitwise{

	//       { & | ^ << >> >>> ~ }
	
	public static void main(String args[]){

		int x=5;
		int y=8;
		int z=-6;

		System.out.println(x&y);
		System.out.println(x|y);
		System.out.println(x^y);
		System.out.println(x>>2);
		System.out.println(y<<3);
		System.out.println(3>>>x);
		System.out.println(~x);
		System.out.println(~z);

	}
}

