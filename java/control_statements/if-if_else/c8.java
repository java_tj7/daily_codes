/*   Electricity bill problem:

   given an input A which represents unites of electricity consumed at your home 
   calculate and print the bill amount.
   
   unites <= 100 -price per unite is 1. 
   unites >100  -price per unite is 2.

   i/p: 50 o/p: 50
   i/p:200 o/p: 300

   */

class electricitbillyprblm{

	public static void main(String[] args){

		int unites= 150;

		if(unites<=100){
			System.out.println(unites*1);
		}else{
			System.out.println((unites-100)*2+100);
			System.out.println((unites*2)-100);
		}
	}
}
